const initialList = ['Wyprowadzić psa', 'Odpisać na maile'];
let textArea, todo, todoBar, todoDate, date, dateText, todoButtons, todoConfirm, todoModify, 
todoDelete, todoText, modalText, modalStyle, modalDisplay, getTitleTodo, getIdTodo, postId;
const apiAdress = "http://195.181.210.249:3000/todo/";
function prepareDOMElements() {
    todoList = document.querySelector('#todoList');
    todoForm = document.querySelector('#todoForm');
    modal = document.querySelector('#modal');
    modalText = document.querySelector('#modalText');

};

async function prepareDOMEvents() {
    todoForm.addEventListener('submit', function(event) {
        event.preventDefault();
        checkModalDisplay ();
        textArea = this.querySelector('input');
        let a = convertString(textArea.value);
        if (textArea.value.trim() !== '' && modalDisplay === "none") {
            const todoFormInput = {
                title : convertString(textArea.value),
            };
            const postTitle = textArea.value;
            textArea.value = '';
            async function postTaskToServer() {
                try {
                    const promisePostTask = await fetch(apiAdress, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(todoFormInput),
                        }
                        );

                    const response = await fetch(apiAdress);
                    const getTaskResponse = await response.json();
                    getTaskResponse.forEach((todo) => {
                        getParams (todo);
                        if (getTitleTodo === postTitle) {
                            postId = getIdTodo;
                        }
                    });
                    createTask(convertString(postTitle), postId);
                    addTask();
                } catch {
                    console.log('Error - add task from input');
                }
            };
            postTaskToServer(); 
        }
        textArea.value = '';
    });

    todoList.addEventListener('click', function(event) {
        checkModalDisplay ();

        if (event.target.closest('.todo-element-confirm') !== null && modalDisplay === "none") {
            checkConfirmButton = event.target.closest('.todo-element');
            checkConfirmButton.classList.add('todo-element-confirm-checked');
            confirmTaskOnServer();
        } else if (event.target.closest('.todo-element-modify') !== null && modalDisplay === "none") {
            checkModifyButton = event.target.closest('.todo-element');
            modalText.value = checkModifyButton.lastChild.innerHTML;
            modal.classList.add('modal-display-on');
            modal.classList.remove('modal-display-off');
             
        } else if (event.target.closest('.todo-element-delete') !== null && modalDisplay === "none") {
            deleteTaskOnServer();
            event.target.closest('.todo-element').remove();
        }
    });

    todoList.addEventListener('dblclick', function(event) {
        checkConfirmTask = event.target.closest('.todo-element');
        checkConfirmTask.classList.add('todo-element-confirm-checked');
        checkConfirmTask.classList.remove('todo-element-modify-checked');
    });

    modal.addEventListener('click', function(event) {
        if (event.target.closest('.modal-button-return') !== null) {
            checkBackButton = event.target.closest('.modal');
            checkBackButton.classList.add('modal-display-off');
            checkBackButton.classList.remove('modal-display-on');
        } else if (event.target.closest('.modal-button-apply') !== null) {
            checkApplyButton = event.target.closest('.todo-element');
            if (checkModifyButton.lastChild.innerHTML.trim() !== '') {
                modifyTaskOnServer();
                modifyTaskLocal(); 
                modalText.value = '';
            } else {
                checkApplyButton = event.target.closest('.modal'); 
                checkApplyButton.classList.add('modal-display-off');
                checkApplyButton.classList.remove('modal-display-on'); 
            }
            checkApplyButton = event.target.closest('.modal'); 
            checkApplyButton.classList.add('modal-display-off');
            checkApplyButton.classList.remove('modal-display-on');
        }
    })
};

function createTask(titleTodo, idTodo, getExtraStatus) {

    todo = document.createElement('div');
    todo.classList.add('todo-element');
    todo.setAttribute("id", idTodo);
    if(getExtraStatus === "active") {
        todo.classList.add('todo-element-confirm-checked');
    };

    todoBar = document.createElement('div');
    todoBar.classList.add('todo-element-bar');

    function minutesFormat() {
        let minutes = date.getMinutes();
        if (minutes < 10) {
            return '0' + minutes;
        } else {
            return minutes;
        };
    };
    
    todoDate = document.createElement('div');
    todoDate.classList.add('todo-element-bar');
    date = new Date();
    dateText = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + ' godz.: ' + date.getHours() + ':' + minutesFormat();
    todoDate.innerHTML = dateText;

    todoButtons = document.createElement('div');
    todoButtons.classList.add('todo-element-buttons');

    todoConfirm = document.createElement('button');
    todoConfirm.classList.add('todo-element-confirm');
    todoConfirm.classList.add('button');
    todoConfirm.innerHTML = '<i class="far fa-check-square"></i>';

    todoModify = document.createElement('button');
    todoModify.classList.add('todo-element-modify');
    todoModify.classList.add('button');
    todoModify.innerHTML = '<i class="far fa-edit"></i>';

    todoDelete = document.createElement('button');
    todoDelete.classList.add('todo-element-delete');
    todoDelete.classList.add('button');
    todoDelete.innerHTML = '<i class="far fa-trash-alt"></i>';

    todoText = document.createElement('div');
    todoText.classList.add('todo-element-text');
    todoText.innerHTML = titleTodo;
};

function addTask() {
    todoButtons.appendChild(todoConfirm);
    todoButtons.appendChild(todoModify);
    todoButtons.appendChild(todoDelete);

    todoBar.appendChild(todoDate);
    todoBar.appendChild(todoButtons);

    todo.appendChild(todoBar);
    todo.appendChild(todoText);

    todoList.appendChild(todo);
};

function deleteTaskOnServer() {
    const id = event.target.closest('.todo-element').id;
    const httpAdress = apiAdress + id;
    fetch(httpAdress, {
        method: 'DELETE',
    })
    .then((response) => response.json())
    .then((postData) => {
        console.log('Success:', postData);
    })
    .catch((error) => {
        console.error('Error:', error);
    });
};

function modifyTaskLocal() {
    const modifyModalText = modalText.value.trim();
    if (modifyModalText !== '') {
        checkModifyButton.lastChild.innerHTML = convertString(modifyModalText);
    } else return
};

function modifyTaskOnServer() {
    const id = checkModifyButton.id;
    const httpAdress = apiAdress + id;
    const modifyText = {
        title : convertString(modalText.value),
    };
    fetch(httpAdress, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(modifyText),
        }
        ).then((response) => response.json())
        .then((postData) => {
          console.log('Success:', postData);
        })
        .catch((error) => {
          console.log('Error:', error);
        });
};

function confirmTaskOnServer() {
    const id = checkConfirmButton.id;
    const httpAdress = apiAdress + id;
    const confirmInfo = {
        extra : "active",
    };
    fetch(httpAdress, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(confirmInfo),
        }
        ).then((response) => response.json())
        .then((postData) => {
          console.log('Success:', postData);
        })
        .catch((error) => {
          console.log('Error:', error);
        });
};

function convertString(text) {
    text = text.replace(/ó/g,'o');
    text = text.replace(/Ó/g,'O');
    text = text.replace(/Ą/g,'A');
    text = text.replace(/ą/g,'a');
    text = text.replace(/Ę/g,'E');
    text = text.replace(/ę/g,'e');
    text = text.replace(/Ł/g,'L');
    text = text.replace(/ł/g,'l');
    text = text.replace(/Ż/g,'Z');
    text = text.replace(/ż/g,'z');
    text = text.replace(/Ź/g,'Z');
    text = text.replace(/ź/g,'z');
    text = text.replace(/Ś/g,'S');
    text = text.replace(/ś/g,'s');
    text = text.replace(/Ć/g,'C');
    text = text.replace(/ć/g,'c');
    return text;
};

function getParams (todo) {
    getTitleTodo = todo.title;
    getIdTodo = todo.id;
    getExtraStatus = todo.extra;
};

function getTasks() {
    fetch("http://195.181.210.249:3000/todo/").
    then((response) => {
        return response.json();
    }).then((value) => {
        value.forEach((todo) => {
            getParams (todo);
            createTask(getTitleTodo, getIdTodo, getExtraStatus);
            addTask();
        }); 
    }).catch(() => {
        console.log("Error - getTasks()");
    }); 
};

function checkModalDisplay () {
    modalStyle = getComputedStyle(modal);
    modalDisplay = modalStyle.display;
    return modalDisplay;
};

function main() {
    getTasks();
    prepareDOMElements();
    prepareDOMEvents();
};

document.addEventListener('DOMContentLoaded', main);